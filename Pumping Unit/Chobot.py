import time
import sys
import RPi.GPIO as GPIO
import json
import threading
import traceback

GPIO.setmode(GPIO.BCM)

FLOW_RATE = 60.0/100.0

class pumps():
	def __init__(self):
		self.pump_configuration = readJsonForPumps()
		for eachPump in self.pump_configuration.keys():
			GPIO.setup(self.pump_configuration[eachPump]["pin"], GPIO.OUT, initial = GPIO.HIGH)
		
	def pump(self,pumpPin, duration):
		GPIO.output(pumpPin, GPIO.LOW)
		time.sleep(duration)
		GPIO.output(pumpPin, GPIO.HIGH)


def readJsonForPumps():
	return json.load(open('pumpDefined.json'))

def pumpTest():
	pumping1 = pumps()
	print('Drinks Available')
	for eachPump in pumping1.pump_configuration.keys():
		pumping1.pump_configuration[eachPump]["value"]
	drinkName = raw_input('What do you want to drink?     ')
	drinkQuantity = input('How much ml?')
	print('Drink dispensing')
	pin = -1
	for eachPump in pumping1.pump_configuration.keys():
		if(pumping1.pump_configuration[eachPump]["value"] == drinkName):
			pin = pumping1.pump_configuration[eachPump]["pin"]
	pumpTime =  (drinkQuantity * FLOW_RATE)
	print(pumpTime)
	print(pin)
	pumping1.pump(pin, pumpTime)
pumpTest()
