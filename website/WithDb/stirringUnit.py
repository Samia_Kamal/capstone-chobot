import time
import sys
import RPi.GPIO as GPIO
import json
import threading
import traceback

def stir(turns):
    GPIO.setmode(GPIO.BCM)
    GPIO.setup(12,GPIO.OUT)
    servo1 = GPIO.PWM(12,50)
    # Note 11 is pin, 50 = 50Hz pulse
    #start PWM running, but with value of 0 (pulse off)
    servo1.start(0)
    control_pins = [19,13,6,5]
    for pin in control_pins:
        GPIO.setup(pin, GPIO.OUT)
        GPIO.output(pin, 0)
    halfstep_seq = [
      [1,0,0,0],
      [1,1,0,0],
      [0,1,0,0],
      [0,1,1,0],
      [0,0,1,0],
      [0,0,1,1],
      [0,0,0,1],
      [1,0,0,1]
    ]
    for i in range(512* turns):
        for halfstep in range(8):

            for pin in range(4):
                GPIO.output(control_pins[pin], halfstep_seq[halfstep][pin])
            time.sleep(0.001)
    GPIO.cleanup()
    return 'ok'
