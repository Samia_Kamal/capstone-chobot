import time
import sys
import RPi.GPIO as GPIO
import json
import threading
import traceback

def pourDrink(startP = 11.75, endP= 5, steps = 1):
	# Set GPIO numbering mode
	GPIO.setmode(GPIO.BCM)

	# Set pin 11 as an output, and set servo1 as pin 11 as PWM
	GPIO.setup(12,GPIO.OUT)
	servo1 = GPIO.PWM(12,50) # Note 11 is pin, 50 = 50Hz pulse

	#start PWM running, but with value of 0 (pulse off)
	servo1.start(0)
	time.sleep(2)

	#Let's move the servo!

	# Define variable duty
	duty = startP
	while duty >= endP:
		print(duty)
		servo1.ChangeDutyCycle(duty)
		time.sleep(0.3)
		servo1.ChangeDutyCycle(0)
		time.sleep(0.7)
		duty = duty - steps

	time.sleep(0.7)
	servo1.ChangeDutyCycle(endP)
	time.sleep(0.7)
	servo1.ChangeDutyCycle(0)
	# Wait a couple of seconds
	time.sleep(10)

	# Turn back to 90 degrees
	duty = endP
	while duty <= startP:
		servo1.ChangeDutyCycle(duty)
		time.sleep(0.3)
		servo1.ChangeDutyCycle(0)
		time.sleep(0.7)
		duty = duty + steps

	# Wait a couple of seconds
	time.sleep(2)

	#Clean things up at the end
	servo1.stop()
	GPIO.cleanup()
	return 'ok'
