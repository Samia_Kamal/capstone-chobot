import time
import sys
import RPi.GPIO as GPIO
import json
import threading
import traceback


def iceDispense(st, end, interval):
    try:
        GPIO.setmode(GPIO.BCM)
        control_pins = [16,20,21,26]
        for pin in control_pins:
            GPIO.setup(pin, GPIO.OUT)
            GPIO.output(pin, 0)
        halfstep_seq = [
          [1,0,0,0],
          [1,1,0,0],
          [0,1,0,0],
          [0,1,1,0],
          [0,0,1,0],
          [0,0,1,1],
          [0,0,0,1],
          [1,0,0,1]
        ]
        for i in range(190):
            for halfstep in range(st, end, interval):
                for pin in range(4):
                    GPIO.output(control_pins[pin], halfstep_seq[halfstep][pin])
                time.sleep(0.001)
        GPIO.cleanup()
        return 'ok'
    except Exception as e:
        return e
